<?php

/*
 * ---------------------------------------------------------------------------------------
 * File: mail.php
 * Created On: Aug 5, 13
 * Created by: Hafiz Haseeb Ali
 * Description: This library uses phpmailer to send emails
 * ---------------------------------------------------------------------------------------
 */
 
/*
	a small mailer script to send spam-free messages
*/
require_once ("libPhpmailer/class.smtp.php");
require_once ("libPhpmailer/class.phpmailer.php");

class mail
{
	var $body;
	var $from;
	var $from_name;
	var $to;
	var $attachment;
	var $subject;
	var $use_smtp 		= true;
	var $base_url 		= "";
	var $images_path 	= "";	
	
	function send_mail()
	{		
		$headers = "From: $this->from \r\n";
		$headers.= "charset=ISO-8859-1 ";
        $random_hash = md5(date('r', time())); 
        $headers .= "\r\nContent-Type: multipart/mixed; boundary=\"PHP-mixed-".$random_hash."\""; 
		$headers .= "MIME-Version: 1.0 ";
        $headers.="Return-Path:<$this->from>\r\n";
		//mail($this->to, $this->subject, $this->body, $headers);
		
		$mail = new PHPMailer();
		if($this->use_smtp==true)
		{
			$CI =& get_instance();
			$settings = $CI->config->item('email');						
			
			$mail->IsSMTP(); // send via SMTP
			$this->mail->CharSet = "utf-8";                  // 一定要設定 CharSet 才能正確處理中文
        $this->mail->SMTPDebug  = 0;                     // enables SMTP debug information
        
        $this->mail->SMTPSecure = "ssl";
			$mail->Host 	= $settings['smtp_host']; 		// SMTP servers
			$mail->Port		= $settings['smtp_port'];
			$mail->SMTPAuth = true;     					// turn on SMTP authentication
			$mail->Username = $settings['smtp_username'];  	// SMTP username
			$mail->Password = $settings['smtp_password']; 	// SMTP password
		}
		
		$mail->From     = $this->from;
		$mail->FromName = $this->from_name;
		$mail->AddAddress($this->to);
		//$mail->AddReplyTo(SMTP_USER,"Information");
		$mail->IsHTML(true);								// send as HTML
		
		$mail->Subject  =  $this->subject;								
		
		if($this->attachment != '')				
			$mail->AddAttachment($this->attachment);
		
		// do we want to send pretty emails :-*
		if (SEND_PRETTY_EMAILS)
		{
			// yes we do :D :D :D
			$pretty_email = "
							<html>
							<head>
								<style>
									/* CSS Document */
									a{
										color:#4276c9;
									}
								</style>
								<title>Web Services</title>
							</head>
							<body style='margin:0px; padding:0px;'>
								<table width='100%' style='background-color:#fff; color:#333; font-family:Arial; font-size:12px;'>
									<tr>
										<td style='margin-bottom:25px;' >
											<!--<img src='". SITE_URL . IMG_PATH . "/strip.png' />-->
										</td>
									</tr>
								<tr>
									<td style='padding-left:10px;'>" . $this->body . "</td>
								</tr>
								<tr>
									<td style='margin-bottom:25px;' >
										&nbsp;
									</td>
								</tr>
								
								<tr>
									<td style='margin-top:25px; height:70px; background-color:#fff; text-align:left;
										padding-left:10px; font-weight:bold; color:#6C6C6C;'>
										<span style='color:#3263AF;'>". SITE_TITLE .".</span>
									</td>
								</tr>
								
								</table>
							</body>
							</html>";
			
			$mail->Body = $pretty_email;
		}
		else
		{
			$mail->Body = $this->body;
		}
		
		if(!$mail->Send())
                {
                
                    file_put_contents('email.json',date('Y-m-d').PHP_EOL.$mail->ErrorInfo.PHP_EOL, FILE_APPEND);
                    return false;
                }
		else
                {
                    return true;
                }
		
	}

}


?>