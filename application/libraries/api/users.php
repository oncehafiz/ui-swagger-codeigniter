<?php

/*
 * ---------------------------------------------------------------------------------------
 * File: users.php
 * Type: Library
 * Created by: Hafiz Haseeb Ali
 * Description: This is base library to handle User functions
 * ---------------------------------------------------------------------------------------
 */

class users
{

    private $CI;

    public function __construct()
    {
        $CI =& get_instance();
        $this->CI = $CI;
    }


    /*
     * Function: 		Login
     * Description: 	Receives post api request and creates user session after validation
     * Params: 			array ( email, password )
     * Written by: 		Hafiz Haseeb Ali
     * Returns: 		sessionId, userId, FirstName, LastName and other details
    */
    public function login($user_info = array())
    {
        $Email = $user_info['email'];
        $Password = $user_info['password'];
        $deviceInfo = $user_info['deviceInfo'];
        if (empty($Email) || empty($Password)) {
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Email or password is missing.";
            return array($response, 400);
        }
        if (empty($deviceInfo)) {
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Device UUID is missing.";
            return array($response, 400);
        }
        $this->CI->load->helper(array('email'));
        /*
            Load all Model
        */

        $this->CI->load->model('User_Model', 'user_model');
        $this->CI->load->model('User_Remote_Session_Model', 'user_remote_session_model');


        $result = $this->CI->user_model->get_record_for_field('email', $Email);

        if (empty ($result)) {
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Invalid email or password";
            return array($response, 400);
        }

        if ($result[0]->password != md5($Password)) {
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Invalid Password";
            return array($response, 400);
        }

        if ($result[0]->status != User_Model::USER_ACTIVE_STATUS) {
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Your account is not activated yet, kindly activate your account first.";
            return array($response, 404);
        }


        /*
            check if session id exists for user id
            then update the previous one with new session id if not then insert it
        */

        $userSessionInfo = $this->CI->user_remote_session_model->get_record_for_field('userId', $result[0]->id);


        if (empty ($userSessionInfo)) {
            $sessionId = $this->generate_session();
            $this->CI->user_remote_session_model->userId = $result[0]->id;
            $this->CI->user_remote_session_model->sessionId = $sessionId;
            $this->CI->user_remote_session_model->createdOn = date("Y-m-d H:i:s");
            $this->CI->user_remote_session_model->updatedOn = date("Y-m-d H:i:s");
            $this->CI->user_remote_session_model->deviceInfo = $deviceInfo;
            $this->CI->user_remote_session_model->save();
        } else {


            $userDeviceInfo = $this->CI->user_remote_session_model->get_record_for_fields(array(
                'deviceInfo' => $deviceInfo,
                'userId' => $result[0]->id));

            if (empty ($userDeviceInfo)) {
                $sessionId = $this->generate_session();
                $this->CI->user_remote_session_model->userId = $result[0]->id;
                $this->CI->user_remote_session_model->sessionId = $sessionId;
                $this->CI->user_remote_session_model->createdOn = date("Y-m-d H:i:s");
                $this->CI->user_remote_session_model->updatedOn = date("Y-m-d H:i:s");
                $this->CI->user_remote_session_model->deviceInfo = $deviceInfo;
                $this->CI->user_remote_session_model->save();
            } else {
                $sessionId = $userSessionInfo[0]->sessionId;
                $this->CI->user_remote_session_model->id = $userSessionInfo[0]->id;
                $this->CI->user_remote_session_model->updatedOn = date("Y-m-d H:i:s");
                $this->CI->user_remote_session_model->deviceInfo = $deviceInfo;
                $this->CI->user_remote_session_model->save();

            }
        }

        $response['result']['status'] = "success";
        $response['result']['response'] = "User logged in successfully.";
        $response['result']['isProvider'] = $result[0]->isProvider;
        $response['result']['type'] = $result[0]->type;
        $response['result']['sessionId'] = trim($sessionId);

        $response['user_data']['id'] = trim($result[0]->id);
        $response['user_data']['isProvider'] = trim($result[0]->isProvider);
        $response['user_data']['firstName'] = trim($result[0]->firstName);
        $response['user_data']['lastName'] = trim($result[0]->lastName);
        $response['user_data']['businessName'] = trim($result[0]->businessName);
        $response['user_data']['businessDescription'] = trim($result[0]->businessDescription);
        $response['user_data']['status'] = trim($result[0]->status);
        $response['user_data']['email'] = trim($result[0]->email);
        $response['user_data']['phoneNumber'] = trim($result[0]->phoneNumber);
        $response['user_data']['lat'] = trim($result[0]->lat);
        $response['user_data']['lon'] = trim($result[0]->lon);
        $response['user_data']['photoPath'] = trim($result[0]->photoPath);
        $response['user_data']['coverPhoto'] = trim($result[0]->coverPhoto);
        $response['user_data']['isReceiveAlert'] = trim($result[0]->isReceiveAlert);
        $response['user_data']['type'] = trim($result[0]->type);
        $response['user_data']['fbId'] = trim($result[0]->fbId);
        $response['user_data']['fbToken'] = trim($result[0]->fbToken);
        $response['user_data']['fbLink'] = trim($result[0]->fbLink);
        $response['user_data']['createdOn'] = trim($result[0]->createdOn);
        $response['user_data']['updatedOn'] = trim($result[0]->updatedOn);




        return array($response, 201);

    }


    /*
     * Function: 		resetPassword
     * Description: 	Receives Email of user and send passsword reset instructions
     * Params: 			array ( email)
     * Written by: 		Hafiz Haseeb
     * Date: 			Jan 1,2014
     * Returns: 		...........
     */
    public function resetPassword($user_info = array())
    {

        $Email = trim($user_info['email']);
        $this->CI->load->helper('Email');
        $this->CI->load->helper('string');

        if ($Email == "" || !valid_email($Email)) {
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Valid Email is missing.";
            return array($response, 400);
        }

        $this->CI->load->model('user_model', 'user');

        $userInfo = $this->CI->user->get_record_for_field('email', $Email);

        if (empty($userInfo)) {
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Email not registered";
            return array($response, 404);
        }

        if ($userInfo[0]->status == 0) {
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Your account is not activated yet or it has been removed.";
            return array($response, 404);
        }

        if ($userInfo[0]->type == User_Model::ACCOUNT_FACEBOOK_TYPE) {
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Unable to reset password. Account is assosiatiated with Facebook.";
            return array($response, 404);
        }



        $newPassword = substr(str_shuffle(str_repeat("0123456T789aAbBcCdDeEfFgGhHiIjJkKlL@#*mnopqrstuvwxyz", 8)), 0, 8);
        // Save new Password in db
        $req_name = $userInfo[0]->firstName . '  ' . $userInfo[0]->lastName;
        $this->CI->user->id = $userInfo[0]->id;
        $this->CI->user->password = md5($newPassword);
        $this->CI->user->updatedOn = date('Y-m-d H:i:s');

        $this->CI->user->save();

        $Email_set = $this->CI->config->item('email');
        // Send Email to user
        $subject = "Password Recovery";
        $body = "Dear $req_name,<br/><br/>";
        $body .= "We have received a new password request for your account. <br/><br/>";
        $body .= "Your new password is {$newPassword} <br/><br/>";
        $body .= "Thank you,<br/>Development Team<br/>";

        $objmail = null;
        $this->CI->load->library('mail');

        $objmail = new mail();
        $objmail->from = 'support@info.com';
        $objmail->from_name = "Sender Name";
        $objmail->subject = $subject;
        $objmail->to = $userInfo[0]->email;
        $objmail->body = $body;
       // $status = $objmail->send_mail() ? 1 : 0;

        $response['result']['status'] = 'success';
        $response['result']['response'] = "An Email has been sent to you.";
        $response['result']['newPassword'] = $newPassword;



        return array($response, 200);

    }

    /*
     * Function: 		changePassword
     * Description: 	changePassword user to the system
     * Params: 			sessionId, OldPassword, NewPassword,ConfirmNewPassword
     * Written by: 		Hafiz Haseeb Ali
     * Date: 			Jan 23, 2014
     * Returns: 		...........
    */
    public function changePassword($user_info = array())
    {
        $sessionId = $user_info['sessionId'];
        // Do basic validation
        $error = "";
        if ($sessionId == "") {
            $error = "Session id is missing";
        }
        if ($error != "") {
            $response['result']['status'] = "error";
            $response['result']['response'] = $error;
            return array($response, 400);
        }

        $this->CI->load->model('user_model', 'user');
        $this->CI->load->model('user_remote_session_model', 'user_remote_sessions');

        // Verify Session
        $userSessionInfo = $this->CI->user_remote_sessions->get_record_for_field('sessionId', $sessionId);
        if (empty ($userSessionInfo)) {
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Invalid Session id";
            return array($response, 400);
        }

        $userSessionInfo = $this->CI->user->get_record_for_field('id', $userSessionInfo[0]->userId);
        $userInfo = array(
            'userId' => $userSessionInfo[0]->id,
            'Password' => $userSessionInfo[0]->password,
            //'City'              =>$userSessionInfo[0]->City,
            //'State'             =>$userSessionInfo[0]->State,
            //'Zip'               =>$userSessionInfo[0]->Zip,
            //'Address'           =>$userSessionInfo[0]->Address,
            'Mobile' => $userSessionInfo[0]->phoneNumber,
            'FirstName' => $userSessionInfo[0]->firstName,
            'LastName' => $userSessionInfo[0]->lastName,
            //'Dob'               =>$userSessionInfo[0]->Dob,
            'Status' => $userSessionInfo[0]->status,
            'PhotoPath' => $userSessionInfo[0]->photoPath,
            'coverPhoto' => $userSessionInfo[0]->coverPhoto
        );
        if ($user_info['newPassword'] == "" && $user_info["confirmNewPassword"] == "" && $user_info['oldPassword'] == "") {
            $response['result']['status'] = "success";
            $response['result']['response'] = "Updated Successfull";
            return array($response, 200);
        } else if (trim($user_info["newPassword"]) == "")
            $error = "New Password is missing";
        else if (trim($user_info["confirmNewPassword"]) == "")
            $error = "Confirm New Password is missing";
        else if (trim($user_info["confirmNewPassword"]) != trim($user_info["newPassword"]))
            $error = "Confirm New Password and New Password does not match";
        else if (strlen(trim($user_info["newPassword"])) < 6)
            $error = "The Password should contain at least 6 characters";
        else if (md5($user_info["oldPassword"]) != $userInfo['Password'])
            $error = "Old Password does not match";
        if ($error != "") {
            $response['result']['status'] = "error";
            $response['result']['response'] = $error;
            return array($response, 400);
        }
        $this->CI->user->id = $userInfo['userId'];
        $this->CI->user->password = md5($user_info["newPassword"]);
        $this->CI->user->updatedOn = date('Y-m-d H:i:s');
        $this->CI->user->save();
        $response['result']['status'] = "success";
        $response['result']['response'] = "Updated Successfull";
        return array($response, 200);
    }



    /*
    * Function: 		logout
    * Description:              logout and delete session
    * Params: 			array ( sessionId)
    * Returns:                  ...........
    */
    public function logout($user_info = array())
    {
        $sessionId = $user_info['sessionId'];
        // Do basic validation
        $error = "";
        if (empty($sessionId)) {
            $error = "Session id is missing";
        }
        if ($error != "") {
            $response['result']['status'] = 'error';
            $response['result']['response'] = $error;
            return array($response, 400);
        }
        $this->CI->load->model('user_remote_session_model', 'user_remote_sessions');
        // Verify Session
        $userSessionInfo = $this->CI->user_remote_sessions->get_record_for_field('sessionId', $sessionId);
        if (empty($userSessionInfo)) {
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Invalid Session id";
            return array($response, 400);
        }
        $this->CI->user_remote_sessions->id = $userSessionInfo[0]->id;
        $this->CI->user_remote_sessions->delete();
        $response['result']['status'] = 'success';
        $response['result']['response'] = "Logout Successfully.";
        return array($response, 201);
    }

    /*
     * Function: 		getProfile
     * Description: 	Get user profile
     * Params: 			array ( sessionId)
     * Returns: 		...........
     */
    public function getProfile($user_info = array())
    {

        $sessionId = $user_info['sessionId'];
        $userId = $user_info['userId'];

        // Do basic validation
        $error = "";
        if (empty($sessionId) and empty($userId)) $error = "Session id is missing";
        if ($error != "") {
            $response['result']['status'] = 'error';
            $response['result']['response'] = $error;
            return array($response, 400);
        }

        $this->CI->load->model('User_Model', 'user');
        $this->CI->load->model('User_Remote_Session_Model', 'user_remote_sessions');
        $this->CI->load->model('User_Profile');
        // Verify Session

        $userSessionInfo = $this->CI->user->get_record_for_field('id', $userId);
        if (empty ($userSessionInfo)) {
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Invalid Session id";
            return array($response, 400);
        }

        $userId = $userSessionInfo[0]->id;
        $result = $this->CI->user->get_record_for_fields(array('id' => $userId, 'status' => User_Model::USER_ACTIVE_STATUS));

        if (!empty($result)) {
            $response['result']['status'] = 'success';
            $response['result']['response'] = "Profile returned successfully.";
            $response['user_data']['current_time'] = date("H:i:s");
            $response['user_data']['LastLogin'] = date("F j,H:i:s", strtotime(trim($userSessionInfo[0]->updatedOn)));

            $response['user_data']['id'] = trim($result[0]->id);
            $response['user_data']['isProvider'] = trim($result[0]->isProvider);
            $response['user_data']['firstName'] = trim($result[0]->firstName);
            $response['user_data']['lastName'] = trim($result[0]->lastName);
            $response['user_data']['businessName'] = trim($result[0]->businessName);

            $response['user_data']['status'] = trim($result[0]->status);
            $response['user_data']['email'] = trim($result[0]->email);
            $response['user_data']['phoneNumber'] = trim($result[0]->phoneNumber);
            $response['user_data']['distance'] = trim($result[0]->distance);
            $response['user_data']['lat'] = trim($result[0]->lat);
            $response['user_data']['lon'] = trim($result[0]->lon);

            $response['user_data']['photoPath'] = trim($result[0]->photoPath);
            $response['user_data']['coverPhoto'] = trim($result[0]->coverPhoto);
            $response['user_data']['isReceiveAlert'] = trim($result[0]->isReceiveAlert);
            $response['user_data']['type'] = trim($result[0]->type);
            $response['user_data']['fbId'] = trim($result[0]->fbId);
            $response['user_data']['fbToken'] = trim($result[0]->fbToken);
            $response['user_data']['fbLink'] = trim($result[0]->fbLink);
            $response['user_data']['createdOn'] = trim($result[0]->createdOn);
            $response['user_data']['updatedOn'] = trim($result[0]->updatedOn);
            $response['user_data']['businessDescription'] = trim($result[0]->businessDescription);


            $user_profiles = $this->CI->User_Profile->get_record_for_field('userId', $result[0]->id);

            if (!empty($user_profiles)) {
                $response['user_data']['imageOne'] = $user_profiles[0]->imageOne;
                $response['user_data']['imageTwo'] = $user_profiles[0]->imageTwo;
                $response['user_data']['imageThree'] = $user_profiles[0]->imageThree;
                $response['user_data']['imageFour'] = $user_profiles[0]->imageFour;
            } else {
                $response['user_data']['imageOne'] = 1;
                $response['user_data']['imageTwo'] = 1;
                $response['user_data']['imageThree'] = 1;
                $response['user_data']['imageFour'] = 1;
            }

            return array($response, 201);
        } else {
            $response['result']['status'] = 'error';
            $response['result']['response'] = "User does not exists.";
            return array($response, 400);
        }

    }


    /**
     * @return string
     * Generate Random string
     */
    private function generate_session()
    {
        $sessionId = sha1(rand(100, 999) . "_session_" . time());
        return $sessionId;
    }



    public function image_decode($str, $path)
    {


        $img = str_replace('data:image/png;base64,', '', $str);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $name = rand() . "_" . date('s') . "_" . date("m") . "_Pretty_" . time() . '.png';
        $file = SERVER_ROOT_PATH . $name;
        $success = file_put_contents($file, $data);

        //var_dump(move_uploaded_file($success,$path));
        return $path . $name;
    }


}	