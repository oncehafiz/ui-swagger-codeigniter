<div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="<?=$site_url?>dashboard" class="simple-text">
               Web Services
            </a>
        </div>

        <ul class="nav">

            <li class="<?=$active_user?>">
                <a href="<?php echo $site_url; ?>users">
                    <i class="pe-7s-note2"></i>
                    <p>User List</p>
                </a>
            </li>

            <li class="<?=$active_notification?>">
                <a href="<?php echo $site_url; ?>notifications">
                    <i class="pe-7s-bell"></i>
                    <p>Notifications</p>
                </a>
            </li>

           <!-- <li>
                <a href="<?php /*echo $site_url; */?>admin/student">
                    <i class="pe-7s-user"></i>
                    <p>Student Profile</p>
                </a>
            </li>
            <li>
                <a href="<?php /*echo $site_url; */?>admin/students_list">
                    <i class="pe-7s-note2"></i>
                    <p>Students List</p>
                </a>
            </li>
            <li>
                <a href="<?php /*echo $site_url; */?>admin/fee_info">
                    <i class="pe-7s-news-paper"></i>
                    <p>Fee Information</p>
                </a>
            </li>
            <li>
                <a href="<?php /*echo $site_url; */?>admin/student_attendance">
                    <i class="pe-7s-science"></i>
                    <p>Attendance</p>
                </a>
            </li>
            <li>
                <a href="maps.html">
                    <i class="pe-7s-map-marker"></i>
                    <p>Maps</p>
                </a>
            </li>
            <li>
                <a href="notifications.html">
                    <i class="pe-7s-bell"></i>
                    <p>Notifications</p>
                </a>
            </li>
            <li class="active-pro">
                <a href="upgrade.html">
                    <i class="pe-7s-rocket"></i>
                    <p>Upgrade to PRO</p>
                </a>
            </li>-->


        </ul>
    </div>
</div>