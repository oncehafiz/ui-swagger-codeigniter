<!--Head is starts from here-->
<?php echo $head; ?>
<!--Head is ends from here-->
</head>


<body class="background_loginpage">

<div class="jumbotron vertical-center" style="background:none;">

    <div class="col-md-3 iPhone-features text-center login_background">
        <div class="wrapper">
            <?php

            if (validation_errors() or $error) {
                ?>
                <div class="alert alert-danger">
                    <a class="close" data-dismiss="alert">x</a>

                        <?php
                          print_r($error);
                          print_r(validation_errors());
                        ?>

                </div>
            <?php } ?>

            <form class="form-signin preetyloginformmain" method="post" action="<?=$site_url?>login"
                <h2 class="form-signin-heading"><img src="<?=$site_url?>assets/img/pronto-circle-img.png"> </h2>
                <input type="email" class="form-control margin_input_preety" name="email" placeholder="Email Address" required="" autofocus="" />
                <input type="password" class="form-control margin_input_preety" name="password" placeholder="Password" required=""/>
                <input type="submit" name="submit" class="button_loginpagesubmit" />
            </form>
        </div>
    </div>
</div>


</body>

<!--Footer Script is starts from here-->
<?php echo $footer_script; ?>
<!--Footer Script is Ends from here-->
</html>
