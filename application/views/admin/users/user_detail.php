<!--Head is starts from here-->
<?php echo $head; ?>
<!--Head is ends from here-->
</head>
<body>

<div class="wrapper">
    <!--Side bar is starts from here-->
    <?php echo $side_bar; ?>
    <!--Side bar is ends from here-->

    <div class="main-panel">
        <!--NavBar is starts from here-->
        <?php echo $nav_bar; ?>
        <!--NavBar is ends from here-->

        <!--Content is ends from here-->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">User Information</h4>

                                <!-- <p class="category">Complete information about user</p>-->
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <tbody>


                                    <tr>
                                        <th>Name</th>
                                        <td><?= $user->firstName ?></td>
                                    </tr>
                                    <?php if ($user->isProvider == 1) { ?>
                                        <tr>
                                            <th>Business</th>
                                            <td><?= $user->businessName ?></td>
                                        </tr>
                                        <tr>
                                            <th>Business Description</th>
                                            <td><?= $user->businessDescription ?></td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <th>Phone Number</th>
                                        <td><?= $user->phoneNumber ?></td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td><?= $user->email ?></td>
                                    </tr>

                                    <tr>
                                        <th>Is Provider</th>
                                        <td>
                                            <?php
                                            if ($user->isProvider == 1) {
                                                echo "Service Provider";
                                            } else {
                                                echo "Customer";
                                            }
                                            ?>

                                        </td>
                                    </tr>
                                    <?php if ($user->isProvider == 0) { ?>
                                        <tr>
                                            <th>Distance Radius</th>
                                            <td><?= $user->distance ?></td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <th>Lat</th>
                                        <td><?= $user->lat ?></td>
                                    </tr>

                                    <tr>
                                        <th>Lon</th>
                                        <td><?= $user->lon ?></td>
                                    </tr>

                                    <!-- <tr>
                                            <th>Activation Code</th>
                                            <td><? /*= $user->activationCode */ ?></td>
                                        </tr>-->

                                    <!--<tr>
                                            <th>Receive Alert</th>
                                            <td><? /*= $user->isReceiveAlert */ ?></td>
                                        </tr>-->
                                    <?php if ($user->type == 1) { ?>
                                        <tr>
                                            <th>Facebook ID</th>
                                            <td><?= $user->fbId ?></td>
                                        </tr>

                                        <!-- <tr>
                                            <th>Facebook Token</th>
                                            <td><? /*= $ft = substr ($user->fbToken, 0, 20) . "..."; */ ?></td>
                                        </tr>-->

                                        <tr>
                                            <th>Facebook Link</th>
                                            <td><a href="<?= $user->fbLink ?>"><?= $user->fbLink ?></a></td>
                                        </tr>

                                    <?php } ?>

                                    <tr>
                                        <th>Created On</th>
                                        <td><?= $user->createdOn ?></td>
                                    </tr>

                                    <tr>
                                        <th>Updated On</th>
                                        <td><?= $user->updatedOn ?></td>
                                    </tr>

                                    <tr>
                                        <th style="vertical-align: middle;">Photo</th>
                                        <td>
                                            <div class="col-md-4">
                                                <img class="img-responsive img-thumbnail"
                                                     style="height: 100%; width: 100%;" src="<?= $user->photoPath ?>"/>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th style="vertical-align: middle;">Cover Photo</th>
                                        <td>
                                            <div class="col-md-4">
                                                <img class="img-responsive img-thumbnail"
                                                     style="height: 100%; width: 100%;" src="<?= $user->coverPhoto ?>"/>
                                            </div>
                                        </td>

                                    </tr>


                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Content is ends from here-->

        <?php if ($user->isProvider == 0) {
            if (count($notifications) > 0) {
                ?>

                <!--Content is ends from here-->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="header">
                                        <h4 class="title">Recent Requests</h4>
                                        <!-- <p class="category">Here is the list of all registered notifications.</p>-->
                                    </div>
                                    <div class="content table-responsive table-full-width user_contant">
                                        <table id="pagination" class="table table-hover table-striped">
                                            <thead>
                                            <th>Sr#</th>
                                            <th>Receiver</th>
                                            <th>Category</th>
                                            <th>Notification</th>
                                            <th>Sent Date</th>

                                            </thead>
                                            <tbody>
                                            <?php $i = 1;
                                            foreach ($notifications as $notification): ?>
                                                <tr>
                                                    <td style="vertical-align: middle;"><?= $i++ ?></td>

                                                    <td style="vertical-align: middle;">
                                                        <img
                                                            class="img-responsive img-thumbnail"
                                                            style="height: 100px; width: 100px;"
                                                            src="<?= $notification->receiver_img ?>"/>
                                                        <?= $notification->receiver_name ?>
                                                    </td>
                                                    <td><img class="img-responsive img-thumbnail"
                                                             style="height: 100px; width: 100px;"
                                                             src="<?= $site_url ?>assets/category/<?= $notification->category_img ?>"/>
                                                    </td>
                                                    <td style="vertical-align: middle;"><?= $notification->notification ?></td>
                                                    <td style="vertical-align: middle;"><?= $notification->createdOn ?></td>

                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Content is ends from here-->

            <?php
            }
            if (count($sps) > 0) {
                ?>

                <!--Content is ends from here-->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="header">
                                        <h4 class="title">Favourite</h4>
                                        <!-- <p class="category">Here is the list of all registered notifications.</p>-->
                                    </div>
                                    <div class="content table-responsive table-full-width user_contant">
                                        <table id="pagination2" class="table table-hover table-striped">
                                            <thead>
                                            <th>Sr#</th>
                                            <th>Service Provider</th>
                                            <th>Email</th>
                                            </thead>
                                            <tbody>
                                            <?php $i = 1;
                                            foreach ($sps as $sp): ?>
                                                <tr>
                                                    <td style="vertical-align: middle;"><?= $i++ ?></td>
                                                    <td><img class="img-responsive img-thumbnail"
                                                             style="height: 100px; width: 100px;"
                                                             src="<?= $sp->sp_img ?>"/>
                                                        <?= $sp->sp_name ?>
                                                    </td>
                                                    <td style="vertical-align: middle;">
                                                        <a href='mailto:<?= $sp->sp_email ?>'><?= $sp->sp_email ?></a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Content is ends from here-->

            <?php
            }
        }
        ?>


        <?php if ($user->isProvider == 1) { ?>


            <!--Content is ends from here-->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="header">
                                    <h4 class="title">Your Business Profile</h4>
                                    <hr>
                                    <!-- <p class="category">Here is the list of all registered notifications.</p>-->
                                </div>

                                <div class="content table-responsive table-full-width user_contant">
                                    <div class="col-md-3"><img class="img-responsive img-thumbnail" style="width: 100%;"
                                                               src="<?= $img->imageOne ?>"/></div>
                                    <div class="col-md-3"><img class="img-responsive img-thumbnail" style="width: 100%;"
                                                               src="<?= $img->imageTwo ?>"/></div>
                                    <div class="col-md-3"><img class="img-responsive img-thumbnail" style="width: 100%;"
                                                               src="<?= $img->imageThree ?>"/></div>
                                    <div class="col-md-3"><img class="img-responsive img-thumbnail" style="width: 100%;"
                                                               src="<?= $img->imageFour ?>"/></div>

                                </div>

                                <div class="content table-responsive table-full-width user_contant">
                                    <?php if (count($cats) > 0): ?>
                                        <?php foreach ($cats as $cat): ?>

                                            <div class="col-md-2">
                                                <img class="img-responsive img-thumbnail"
                                                     style="height: 100%; width: 100%;"
                                                     src="<?= $site_url ?>assets/category/<?= $cat->img ?>"/>
                                            </div>

                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Content is ends from here-->



            <?php if (count($notifications) > 0) { ?>

                <!--Content is ends from here-->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="header">
                                        <h4 class="title">Recent Received</h4>
                                        <!-- <p class="category">Here is the list of all registered notifications.</p>-->
                                    </div>
                                    <div class="content table-responsive table-full-width user_contant">
                                        <table id="pagination2" class="table table-hover table-striped">
                                            <thead>
                                            <th>Sr#</th>
                                            <th>Image</th>
                                            <th>Sender</th>
                                            <th>Category</th>
                                            <th>Notification</th>
                                            <th>Sent On</th>

                                            </thead>
                                            <tbody>
                                            <?php $i = 1;
                                            foreach ($notifications as $notification): ?>
                                                <tr>
                                                    <td style="vertical-align: middle;"><?= $i++ ?></td>
                                                    <td><img class="img-responsive img-thumbnail"
                                                             style="height: 100px; width: 100px;"
                                                             src="<?= $notification->sender_img ?>"/></td>
                                                    <td style="vertical-align: middle;"><?= $notification->sender_name ?></td>
                                                    <td><img class="img-responsive img-thumbnail"
                                                             style="height: 100px; width: 100px;"
                                                             src="<?= $site_url ?>assets/category/<?= $notification->category_img ?>"/>
                                                    </td>
                                                    <td style="vertical-align: middle;"><?= $notification->notification ?></td>
                                                    <td style="vertical-align: middle;"><?= $notification->createdOn ?></td>


                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Content is ends from here-->

            <?php
            }
        }
        ?>




        <!--Footer is starts from here-->
        <?php echo $footer; ?>
        <!--Footer is Ends from here-->

    </div>
</div>


</body>
<!--Footer Script is starts from here-->
<?php echo $footer_script; ?>
<!--Footer Script is Ends from here-->
</html>
