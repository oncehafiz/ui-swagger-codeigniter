<!--Head is starts from here-->
<?php echo $head; ?>
<!--Head is ends from here-->
</head>
<body>

<div class="wrapper">
    <!--Side bar is starts from here-->
    <?php echo $side_bar; ?>
    <!--Side bar is ends from here-->

    <div class="main-panel">
        <!--NavBar is starts from here-->
        <?php echo $nav_bar; ?>
        <!--NavBar is ends from here-->



        <!--Content is ends from here-->
        <div class="content">
            <div class="container-fluid">

                <?php if(!empty($msg)){ ?>
                <div class="alert alert-success">
                    <a class="close" data-dismiss="alert">x</a>
                   <?=$msg?>
                </div>
               <?php } ?>

                <!--tabs start-->
                <div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="<?= $active_customer ?>" ><a href="#customer" aria-controls="home" role="tab" data-toggle="tab">Customer</a></li>
                        <li role="presentation" class="<?= $active_sp ?>" ><a href="#sp" aria-controls="profile" role="tab" data-toggle="tab">Service Provider</a></li>
                       <!-- <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
                        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>-->
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane <?= $active_customer ?>" id="customer">

                         <!--Main start-->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="header">
                                            <!--<h4 class="title header_title">Users List</h4>-->
                                            <!--<p class="category">Here is the list of all registered users.</p>-->
                                        </div>
                                        <div class="content table-responsive table-full-width user_contant">
                                            <table id="pagination" class="table table-hover table-striped" style="width: 100%;">
                                                <thead>
                                                <th>Sr#</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if(count($customers) > 0):
                                                    $i=1;
                                                foreach($customers as $customer):

                                                    ?>
                                                    <tr>
                                                        <td><?= $i++?></td>
                                                        <td><?= $customer->firstName ?></td>
                                                        <td><?= $customer->email ?></td>
                                                        <td><?= $customer->phoneNumber ?></td>
                                                        <td>
                                                            <!-- <span class="label label-default">Default</span>
                                                                   <span class="label label-primary">Primary</span>
                                                            <span class="label label-info">Info</span>-->

                                                            <?php if($customer->status==1){ ?>
                                                                <span class="label label-success">Activated</span>

                                                            <?php }elseif($customer->status==2){ ?>
                                                                <span class="label label-danger">Deactivated</span>
                                                            <?php  }elseif($customer->status==3){  ?>
                                                                <span class="label label-warning">Pending...</span>
                                                            <?php  } ?>
                                                        </td>
                                                        <td>

                                                            <a href="customer/detail/<?= $customer->id ?>" title="View">
                                                                <img src="<?=$site_url?>/assets/img/icons/view.png">
                                                            </a>
                                                            <a href="user/edit/<?= $customer->id ?>" title="Edit">
                                                                <img src="<?=$site_url?>/assets/img/icons/edit.png">
                                                            </a>
                                                            <?php if($customer->status==1){ ?>

                                                                <a href="user/deactivate/<?= $customer->id ?>"
                                                                   onclick="return confirm('Are you sure you want to deactivate this customer?');"
                                                                   title="Deactivate">
                                                                    <img src="<?=$site_url?>/assets/img/icons/deactivate.png">
                                                                </a>

                                                            <?php }elseif($customer->status==2 || $customer->status==3){ ?>

                                                                <a href="user/activate/<?= $customer->id ?>"
                                                                   onclick="return confirm('Are you sure you want to activate this customer?');"
                                                                   title="Activate">
                                                                    <img src="<?=$site_url?>/assets/img/icons/activate.png">
                                                                </a>
                                                            <?php  } ?>
                                                            <!-- <a href="user/delete/<?/*= $user->id */?>"
                                               onclick="return confirm('Are you sure you want to delete this item?');"
                                               title="Delete">
                                                <img src="<?/*=$site_url*/?>/assets/img/icons/del.png">
                                                </a>-->
                                                        </td>
                                                    </tr>
                                                <?php
                                                endforeach;
                                                endif;
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         <!--Main end-->
                        </div>

                        <div role="tabpanel" class="tab-pane <?= $active_sp ?>" id="sp">
                            <!--Main start-->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="header">
                                         <!-- <h4 class="title header_title">Users List</h4>-->
                                            <!--<p class="category">Here is the list of all registered users.</p>-->
                                        </div>
                                        <div class="content table-responsive table-full-width user_contant">
                                            <table id="pagination2" class="table table-hover table-striped" style="width: 100%;">
                                                <thead>
                                                <th>Sr#</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if(count($sps) > 0):
                                                    $i=1;
                                                foreach($sps as $sp):
                                                    ?>
                                                    <tr>
                                                        <td><?= $i++?></td>
                                                        <td><?= $sp->firstName ?></td>
                                                        <td><?= $sp->email ?></td>
                                                        <td><?= $sp->phoneNumber ?></td>
                                                        <td>
                                                            <!-- <span class="label label-default">Default</span>
                                                                   <span class="label label-primary">Primary</span>
                                                            <span class="label label-info">Info</span>-->

                                                            <?php if($sp->status==1){ ?>
                                                                <span class="label label-success">Activated</span>

                                                            <?php }elseif($sp->status==2){ ?>
                                                                <span class="label label-danger">Deactivated</span>
                                                            <?php  }elseif($sp->status==3){  ?>
                                                                <span class="label label-warning">Pending...</span>
                                                            <?php  } ?>
                                                        </td>
                                                        <td>

                                                            <a href="service_provider/detail/<?= $sp->id ?>" title="View">
                                                                <img src="<?=$site_url?>/assets/img/icons/view.png">
                                                            </a>
                                                            <a href="user/edit/<?= $sp->id ?>" title="Edit">
                                                                <img src="<?=$site_url?>/assets/img/icons/edit.png">
                                                            </a>

                                                            <?php if($sp->status==1){ ?>

                                                                <a href="user/deactivate/<?= $sp->id ?>"
                                                                   onclick="return confirm('Are you sure you want to deactivate this sp?');"
                                                                   title="Deactivate">
                                                                    <img src="<?=$site_url?>/assets/img/icons/deactivate.png">
                                                                </a>

                                                            <?php }elseif($sp->status==2 || $sp->status==3){ ?>

                                                                <a href="user/activate/<?= $sp->id ?>"
                                                                   onclick="return confirm('Are you sure you want to activate this sp?');"
                                                                   title="Activate">
                                                                    <img src="<?=$site_url?>/assets/img/icons/activate.png">
                                                                </a>
                                                            <?php  } ?>

                                                            <!-- <a href="user/delete/<?/*= $user->id */?>"
                                               onclick="return confirm('Are you sure you want to delete this item?');"
                                               title="Delete">
                                                <img src="<?/*=$site_url*/?>/assets/img/icons/del.png">
                                            </a>-->
                                                        </td>
                                                    </tr>
                                                <?php
                                                endforeach;
                                                endif;
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Main end-->
                        </div>

                    </div>
                       <!-- <div role="tabpanel" class="tab-pane" id="messages">...fsdff</div>
                        <div role="tabpanel" class="tab-pane" id="settings">...gsdgs</div>-->
                    </div>

                </div>
                <!--tabs end-->


            </div>
        </div>
        <!--Content is ends from here-->


        <!--Footer is starts from here-->
        <?php echo $footer; ?>
        <!--Footer is Ends from here-->

    </div>
</div>


</body>
<!--Footer Script is starts from here-->
<?php echo $footer_script; ?>


<!--Footer Script is Ends from here-->
</html>
