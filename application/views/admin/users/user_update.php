<!--Head is starts from here-->
<?php echo $head; ?>
<!--Head is ends from here-->
</head>
<body>

<div class="wrapper">
    <!--Side bar is starts from here-->
    <?php echo $side_bar; ?>
    <!--Side bar is ends from here-->

    <div class="main-panel">
        <!--NavBar is starts from here-->
        <?php echo $nav_bar; ?>
        <!--NavBar is ends from here-->

        <!--Content is ends from here-->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Update User Information</h4>
                            </div>
                            <div class="content">
                                <form id="new_student_form" action="<?=$site_url;?>user/update" method="POST">
                                    <input type="hidden" name="id" value="<?=$user->id?>">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" class="form-control" name="firstName"
                                                  required value="<?=$user->firstName?>"
                                                       id="firstName" placeholder="Enter First Name">
                                            </div>
                                        </div>
                                    </div>
                                  <?php  if($user->isProvider == 1){ ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Business Name</label>
                                                <input type="text" class="form-control" name="businessName"
                                                       required    value="<?=$user->businessName?>"
                                                       id="businessName" placeholder="Enter Business Name">
                                            </div>
                                        </div>
                                    </div>

                                      <div class="row">
                                          <div class="col-md-12">
                                              <div class="form-group">
                                                  <label>Business Description</label>
                                          <textarea rows="4" style="resize: none;" class="form-control" name="businessDescription"
                                                    required id="businessDescription"><?=$user->businessDescription?></textarea>
                                              </div>
                                          </div>
                                      </div>

                                  <?php } ?>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Phone Number</label>
                                                <input type="text" class="form-control" name="phoneNumber"
                                                       required    value="<?=$user->phoneNumber?>"
                                                       id="phoneNumber" placeholder="Enter Phone Number">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Latitude</label>
                                                <input type="text" class="form-control" name="lat"
                                                       required  value="<?=$user->lat?>"
                                                       id="lat" placeholder="Enter Latitude">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Longitude</label>
                                                <input type="text" class="form-control" name="lon"
                                                       required  value="<?=$user->lon?>"
                                                       id="lon" placeholder="Enter Longitude">
                                            </div>
                                        </div>
                                    </div>





                                    <input type="submit" class="btn btn-info custom-btn pull-right" value="Update" style="background: #2ab2da;color:#fff;border: 1px solid;">
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!--Content is ends from here-->
        <div class="clearfix"></div>


        <!--Content is ends from here-->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Update Profile Photo</h4>
                                <hr>
                            </div>
                            <div class="content">

                                <?php echo form_open_multipart('profile/photo/uploaded');?>
                                <input type="hidden" name="id" value="<?=$user->id?>">
                                    <img class="img-responsive img-thumbnail" style="height: 200px; width: 200px;" src="<?= $user->photoPath ?>" />
                                <div class="col-md-12">
                                    <input type="file" class="btn btn-info pull-right" name="userfile" style="background: #2ab2da;color:#fff;">
                                    <input type="submit" class="btn btn-info pull-right" value="Update" style="background: #2ab2da;color:#fff;height: 36px;">
                                </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!--Content is ends from here-->
        <div class="clearfix"></div>

        <!--Content is ends from here-->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Update Cover Photo</h4>
                                <hr>
                            </div>
                            <div class="content">
                                <?php echo form_open_multipart('cover/photo/uploaded');?>
                                    <input type="hidden" name="id" value="<?=$user->id?>">
                                    <img class="img-responsive" style="height: 200px; width: 200px;" src="<?= $user->coverPhoto ?>" />
                                <div class="col-md-12">
                                    <input type="file" class="btn btn-info pull-right" name="userfile" style="background: #2ab2da;color:#fff;">
                                    <input type="submit" class="btn btn-info pull-right" value="Update" style="background: #2ab2da;color:#fff; height: 36px;">

                                </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!--Content is ends from here-->




        <!--Footer is starts from here-->
        <?php echo $footer; ?>
        <!--Footer is Ends from here-->

    </div>
</div>


</body>
<!--Footer Script is starts from here-->
<?php echo $footer_script; ?>
<!--Footer Script is Ends from here-->
</html>
