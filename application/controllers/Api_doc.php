<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * ------------------------------------------------removeuse---------------------------------------
 * File: api_doc.php
 * Type: Controller
 * Created by: Hafiz Haseeb Ali
 * Description: This is controller to handle User functions
 * ---------------------------------------------------------------------------------------
 */

require APPPATH . 'libraries/api/REST_Controller.php';

class Api_doc extends CI_Controller
{

    public $base_path = API_SWAGGER_PATH;

    /*
     * Function: 		index
     * Method Accepted:	post
     * URI: 			/api_doc/
     * Params: 			.....
     * URI Segments: 	None
     * Written by: 		Haseeb Ali
     * Description: 	This is for Swagger documentation
     * Returns: 		Swagger Json Array
     */
    function index()
    {
        $allvalue = array(
            "apiVersion" => "1.0.0",
            "swaggerVersion" => "1.2",
            "apis" => array(
                array(
                    "path" => "/user",
                    "description" => "User Functions"
                )
            ),
            "info" => array(
                "title" => "Restful API",
                "description" => "Below is the list of available REST API functions",

            )
        );
        header('Access-Control-Allow-Origin: *');
        echo json_encode($allvalue);

    }



    /*
     * Function: 		user
     * URI: 			/api_doc/user
     * Params: 			.....
        * Description: 	User Activity
     * Returns: 		Swagger Json Array
     */
    public function user()
    {
        $user = array(
            "apiVersion" => "1.0.0",
            "swaggerVersion" => "1.2",
            "basePath" => $this->base_path,
            "resourcePath" => "/user",
            "produces" => array("application/json"),
            "apis" => array(
                // all about login detail in Swagger Documentation and Testing
                array(
                    "path" => "/user/login",
                    "operations" => array(
                        array(
                            "method" => "POST",
                            "summary" => "Logs user into the system",
                            "notes" => "",
                            "type" => "string",
                            "nickname" => "login",
                            "parameters" => array(
                                array(
                                    "name" => "email",
                                    "description" => "User email for login",
                                    "required" => true,
                                    "type" => "string",
                                    "paramType" => "form"
                                ),
                                array(
                                    "name" => "password",
                                    "description" => "The Password for login in clear text",
                                    "required" => true,
                                    "type" => "string",
                                    "paramType" => "form"
                                ),
                                array(
                                    "name" => "deviceInfo",
                                    "description" => "UUID of the device.",
                                    "required" => true,
                                    "type" => "string",
                                    "paramType" => "form"
                                )
                            ),
                            "responseMessages" => array(
                                array(
                                    "code" => 400,
                                    "message" => "Invalid username and Password combination"
                                )
                            )
                        )
                    )
                ),
                array(
                    "path" => "/user/resetPassword",
                    "operations" => array(
                        array(
                            "method" => "POST",
                            "summary" => "Password Reset",
                            "notes" => "",
                            "type" => "string",
                            "nickname" => "resetPassword",
                            "parameters" => array(
                                array(
                                    "name" => "email",
                                    "description" => "The email for reset Password",
                                    "required" => true,
                                    "type" => "string",
                                    "paramType" => "form"
                                )
                            )
                        )
                    )
                ),
                array(
                    "path" => "/user/changePassword",
                    "operations" => array(
                        array(
                            "method" => "POST",
                            "summary" => "Change Password",
                            "notes" => "",
                            "type" => "string",
                            "nickname" => "changePassword",
                            "parameters" => array(
                                array(
                                    "name" => "sessionId",
                                    "description" => "SessionId of User",
                                    "required" => true,
                                    "type" => "string",
                                    "paramType" => "form"
                                ),
                                array(
                                    "name" => "oldPassword",
                                    "description" => "Old Password of User",
                                    "required" => true,
                                    "type" => "string",
                                    "paramType" => "form"
                                ),
                                array(
                                    "name" => "newPassword",
                                    "description" => "NewPassword of User",
                                    "required" => true,
                                    "type" => "string",
                                    "paramType" => "form"
                                ),
                                array(
                                    "name" => "confirmNewPassword",
                                    "description" => "ConfirmNewPassword of User",
                                    "required" => true,
                                    "type" => "string",
                                    "paramType" => "form"
                                )
                            )
                        )
                    )
                ),
                array(
                    "path" => "/user/getProfile",
                    "operations" => array(
                        array(
                            "method" => "GET",
                            "summary" => "Get Profile",
                            "notes" => "",
                            "type" => "string",
                            "nickname" => "getProfile",
                            "parameters" => array(
                                array(
                                    "name" => "sessionId",
                                    "description" => "The session id for user",
                                    "required" => false,
                                    "type" => "string",
                                    "paramType" => "query"
                                )
                            )
                        )
                    )
                ),
                array(
                    "path" => "/user/logout",
                    "operations" => array(
                        array(
                            "method" => "GET",
                            "summary" => "Logout",
                            "notes" => "",
                            "type" => "string",
                            "nickname" => "logout",
                            "parameters" => array(
                                array(
                                    "name" => "sessionId",
                                    "description" => "The session id for user",
                                    "required" => true,
                                    "type" => "string",
                                    "paramType" => "query"
                                )
                            )
                        )
                    )
                ),
            ),
        );
        header('Access-Control-Allow-Origin: *');
        echo json_encode($user);
    }




}