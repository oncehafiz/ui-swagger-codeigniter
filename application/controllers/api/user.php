<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * ---------------------------------------------------------------------------------------
 * File: user.php
 * Type: Controller
 * Created by: Hafiz Haseeb
 * Description: This is controller to handle User functions
 * ---------------------------------------------------------------------------------------
*/

require APPPATH.'libraries/api/REST_Controller.php';

class User extends REST_Controller
{
	
	/*
	 * Function: 		Login
	 * Method Accepted:	post
	 * URI: 			/api/user/login 
	 * Params: 			email, password
	 * URI Segements: 	None
     * Created by:      Hafiz Haseeb
	 * Description: 	Receives post api request and creates user session after validation
	 * Returns: 		session_id, userid, user first name, user last name, user phone number and other details
    */
	public function login_post()
    {
        
		$data = $this->_post_args;
		try 
        {	
			$this->load->library("api/users");		            
			$loginResult = $this->users->login($data);
			
		} 
        catch (Exception $e) 
        {
			$response['result']['status']		 = 'error';
			$response['result']['response']		 = $e->getMessage();
			$this->response($response, $e->getCode());
		}
		header("Access-Control-Allow-Origin: *");
		$this->response($loginResult[0], $loginResult[1]);

    }
	

	
	/*
	 * Function: 		ResetPassword_post
	 * Method Accepted:	post
	 * URI: 			/api/user/RetrievePassword 
	 * Params: 			email
	 * URI Segements: 	None
     * Created by:      Hafiz Haseeb
	 * Description: 	Receives post api request and does reset password for user
	 * Returns: 		..............
	 */
	public function resetPassword_post()
    {
		$data = $this->_post_args;	
		try 
        {
			$this->load->library("api/users");		            
			$result = $this->users->ResetPassword( $data );	
		} 
        catch (Exception $e) 
        {	
			$response['result']['status']		 = 'error';
			$response['result']['response']		 = $e->getMessage();
			$this->response( $response, $e->getCode() );
		}
		header("Access-Control-Allow-Origin: *");
		$this->response( $result[0], $result[1] );

    }
    

    
    /*
	 * Function: 		changePassword_post
	 * Method Accepted:	post
	 * URI: 			/api/user/changePassword 
	 * Params: 			sessionId, oldPassword, newPassword,confirmNewPassword 
	 * URI Segements: 	None
     * Created by:      Hafiz Haseeb
	 * Description: 	Receives post api request and does change password for user after validation
	 * Returns: 		..............
	 */
	public function ChangePassword_post()
    {
		
		$data = $this->_post_args;
		
		try 
        {	
			$this->load->library( "api/users" );		            
			$result = $this->users->ChangePassword( $data );
		
			
		} 
        catch (Exception $e) 
        {	
			$response['result']['status']		 = 'error';
			$response['result']['response']		 = $e->getMessage();
			$this->response( $response, $e->getCode() );
		}
		header("Access-Control-Allow-Origin: *");
		$this->response( $result[0], $result[1] );

    }
    
    /*
	 * Function: 		Logout get
	 * Description: 	Logout and delete session
	 * Method Accepted:	GET
	 * URI: 			/api/user/logout 
	 * Params: 			session_id
	 * URI Segements: 	None
	 * Written by: 		Muhammad Umar Hayat
	 * Returns: 		...................
	 */
	public function logout_get()
	{
            $params['sessionId'] = $this->_get('sessionId');	
            try 
            {
                $this->load->library("api/users");		            
                $result = $this->users->logout($params);
            }catch(Exception $e) 
            {

                $response['result']['status']	= 'error';
                $response['result']['response']	= $e->getMessage();
                $this->response( $response, $e->getCode() );
            }
            header("Access-Control-Allow-Origin: *");
            $this->response( $result[0], $result[1] );
        }
	
	/*
	 * Function: 		GetProfile get
	 * Description: 	Get profile details
	 * Method Accepted:	GET
	 * URI: 			/api/user/GetProfile 
	 * Params: 			session_id
	 * URI Segements: 	None
	 * Written by: 		Hafiz Haseeb Ali
	 * Returns: 		...................
	 */
	public function getProfile_get()
	{		
	   
		$params['sessionId'] = $this->_get('sessionId');
		$params['userId'] = $this->_get('userId');		
		
		try 
        {
			$this->load->library( "api/users" );		            
			$result = $this->users->GetProfile( $params );		
			
		} catch (Exception $e) {
			
			$response['result']['status']	= 'error';
			$response['result']['response']	= $e->getMessage();
		
			$this->response( $response, $e->getCode() );
		}
		header("Access-Control-Allow-Origin: *");
		$this->response( $result[0], $result[1] );
			
	}


    

	
}