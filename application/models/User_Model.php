<?php

/*
 * ---------------------------------------------------------------------------------------
 * File: user_model.php
 * Created by: Hafiz Haseeb Ali
 * Description: User model
 *
 * ---------------------------------------------------------------------------------------
 */

require_once(APPPATH . 'models/__base.php');

class User_Model extends __Base_Model
{
    public $id;
    public $email;
    public $password;
    public $firstName;
    public $lastName;
    public $businessName;
    public $phoneNumber;
    public $type;
    //public $address;
    public $lat;
    public $lon;
    public $manualLat;
    public $manualLon;
    public $distance;
    public $createdOn;
    public $updatedOn;
    public $status;
    public $photoPath;
    public $coverPhoto;
    public $isReceiveAlert;
    public $activationCode;
    //public $activatedOn;
    //public $resetCode;    
    public $isProvider;
    public $fbId;
    public $fbToken;
    public $fbLink;
    public $businessDescription;

    const USER_ACTIVE_STATUS = 1;
    const USER_DEACTIVE_STATUS = 2;
    const USER_EMAIL_PENDING_VERIFICATION_STATUS = 3;
    const ACCOUNT_FACEBOOK_TYPE = 1;
    const ACCOUNT_USER_TYPE = 0;
    const USER_IS_SERVICE_PROVIDER = 1;
    const USER_IS_NORMAL_USER = 0;
    const USER_BASE_FOR_PHOTO = "http://prosoftsol.com/prettypronto/dev/assets/all_images/place_holder_img@3x.png";
    const USER_BASE_FOR_COVER = "http://prosoftsol.com/prettypronto/dev/assets/all_images/tob_header_bg.png";

    public function __construct($id = null, $email = null, $password = null, $lat = null, $distance = null, $lon = null, $address = null, $phoneNumber = null,$coverPhoto = null, $businessDescription = null,
                                $firstName = null, $lastName = null, $type = null, $status = null, $createdOn = null, $updatedOn = null,
                                $photoPath = null, $isReceiveAlert = null, $activationCode = null , $resetCode = null , $activatedOn = null
    )
    {
        parent::__construct();

        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
        $this->activationCode = $activationCode;
        $this->resetCode = $resetCode;
        $this->activatedOn = $activatedOn;
        $this->lat = $lat;
        $this->distance = $distance;
        $this->lon = $lon;
        $this->address = $address;
        $this->phoneNumber = $phoneNumber;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->type = $type;
        $this->status = $status;
        $this->createdOn = $createdOn;
        $this->updatedOn = $updatedOn;
        $this->photoPath = $photoPath;
        $this->coverPhoto = $coverPhoto;
        $this->isReceiveAlert = $isReceiveAlert;
        $this->businessDescription = $businessDescription;
    }

    public function getTableName()
    {
        return "user";
    }


    /*
     * To save the data in the table which is passed to the class variables
     */
    public function save($data = array())
    {
        if (count($data) == 0) {
            /**
             * Get Class Variables and Only take the not null data
             * ====================================================
             */
            $data = array();
            $class_vars = get_class_vars(get_class($this));
            foreach ($class_vars as $name => $value) {
                if (!is_null($this->$name) AND $name != '_parent_name' AND $name != 'id') {
                    $data[$name] = $this->$name;
                }
            }
            /**
             * ----------------------------------------------------------------------
             */
        }
        if ($this->id == '') {
            $this->db->insert($this->getTableName(), $data);
            return $this->db->insert_id();
        } else {
            $this->db->where('id', $this->id);
            return $this->db->update($this->getTableName(), $data);
        }
    }


    /*
         To delete the row from the table who's Id is given
    */
    public function delete()
    {
        if ($this->id != '') {
            $this->db->where('id = (' . $this->id . ')');
            $this->db->delete($this->getTableName());
        }
    }

    /**
     * @param $email
     * @return bool
     * Check email and existance of email.
     */
    public function validate_email( $email )
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            $response = $this->record_count(" email='$email' ");
            return !($response > 0);
        } else {
            return false;
        }
    }

    /**
     * @param $name
     * @return bool
     * Check user name is valid or not
     */
    public function  validate_name( $name )
    {
        if( $name == '' )
        {
            return false;
        } else {
            $matches = array();
            preg_match('/^([a-zA-Z\' ]*)$/',$name,$matches);
            return count($matches) != 0;
        }
    }

    /**
     * @param $phoneNumber
     * @return bool
     * Check user mobile number is valid or not
     */
    public function validate_mobile( $phoneNumber )
    {
        if( $phoneNumber == '' )
        {
            return false;
        }
        if($phoneNumber != '')
        {
            $matches = array();
            $Phone_Pattern = "/(\d)?(\s|-)?(\()?(\d){3}(\))?(\s|-){1}(\d){3}(\s|-){1}(\d){4}/";
            preg_match($Phone_Pattern,$phoneNumber,$matches);
            return count($matches) != 0;
        }
        return true;

    }
    


}