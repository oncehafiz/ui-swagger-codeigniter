<?php

/*
 * ---------------------------------------------------------------------------------------
 * File: user_profile.php
 * Created by: Hafiz Haseeb Ali
 * Description: User profile
 *
 * ---------------------------------------------------------------------------------------
 */

require_once(APPPATH . 'models/__base.php');

class User_Profile extends __Base_Model
{
    public $id;
    public $userId;
    public $createdOn;
    public $updatedOn;
    public $imageTwo;
    public $imageOne;
    public $imageThree;
    public $imageFour;

    
    const USER_BASE_FOR_PHOTO   = IMAGE_PATH_USER;
    const USER_BASE_FOR_DEFAULT = User_Profile::USER_BASE_FOR_PHOTO;
   
   
    public function __construct($id = null,  $createdOn = null, $updatedOn = null, $userId = null,
                                $imageTwo = null, $imageThree = null, $imageOne = null , $imageFour = null 
    )
    {
        parent::__construct();

        $this->id = $id;
        $this->createdOn  = $createdOn;
        $this->updatedOn  = $updatedOn;
        $this->imageTwo   = $imageTwo;
        $this->imageThree = $imageThree;
        $this->imageOne   = $imageOne;
        $this->imageFour  = $imageFour;
        $this->userId     = $userId;
        
    }

    public function getTableName()
    {
        return "user_profile";
    }


    /*
     * To save the data in the table which is passed to the class variables
     */
    public function save($data = array())
    {
        if (count($data) == 0) {
            /**
             * Get Class Variables and Only take the not null data
             * ====================================================
             */
            $data = array();
            $class_vars = get_class_vars(get_class($this));
            foreach ($class_vars as $name => $value) {
                if (!is_null($this->$name) AND $name != '_parent_name' AND $name != 'id') {
                    $data[$name] = $this->$name;
                }
            }
            /**
             * ----------------------------------------------------------------------
             */
        }
        if ($this->id == '') {
            $this->db->insert($this->getTableName(), $data);
            return $this->db->insert_id();
        } else {
            $this->db->where('id', $this->id);
            return $this->db->update($this->getTableName(), $data);
        }
    }


    /*
         To delete the row from the table who's Id is given
    */
    public function delete()
    {
        if ($this->id != '') {
            $this->db->where('id = (' . $this->id . ')');
            $this->db->delete($this->getTableName());
        }
    }

}