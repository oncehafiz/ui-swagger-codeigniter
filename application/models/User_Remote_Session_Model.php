<?php

/*
 * ---------------------------------------------------------------------------------------
 * File: user_remote_session]_model.php
 * Created By: Hafiz Haseeb
 * Description: Remote Session Model
 *              
 * ---------------------------------------------------------------------------------------
 */
 
require_once( APPPATH . 'models/__base.php');

class User_Remote_Session_Model extends __Base_Model
{
	
	public $id;
	public $userId;
	public $sessionId;
	public $createdOn;
	public $updatedOn;
	public $deviceInfo;
	
	public function __construct($id=null, $userId=null, $sessionId=null, 
								$createdOn=null, $updatedOn=null,$deviceInfo=null )
	{
		parent::__construct();
		
		$this->id    =	$id;
		$this->userId			=	$userId;
		$this->sessionId		=	$sessionId;
		$this->createdOn 		= 	$createdOn;
		$this->updatedOn 	= 	$updatedOn;
        $this->deviceInfo       =   $deviceInfo;

		
	}
	
	public function getTableName()
	{
		return "user_session";	
	}
	
	
	/*
     * To save the data in the table which is passed to the class variables
     */
	public function save( $data = array() )
    {
        if(count($data)==0)
        {
			/**
			 * Get Class Variables and Only take the not null data
			 * ====================================================
			 */
				$data = array();
				$class_vars = get_class_vars(get_class($this));
				foreach ($class_vars as $name => $value)
				{
					if(!is_null($this->$name) AND $this->$name!='id')
					{
						$data[$name]=$this->$name;
					}
				}
			/**
			 * ----------------------------------------------------------------------
			 */
        }

        if($this->id=='')
        {
            $this->db->insert($this->getTableName(), $data);
            $this->db->insert_id();
        }
        else
        {
			
            $this->db->where( 'id', $this->id);
            $this->db->update($this->getTableName(), $data);
        }
    }


    /*
         To delete the row from the table who's Id is given
    */
    public function delete(  )
    {
        if($this->id!='')
        {
            $this->db->where('id', $this->id);
            $this->db->delete($this->getTableName());
        }
    }

}